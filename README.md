# README #

This is Patrick Alcantaras' Job Adder Coding Challenge

In order to run the api please go into api/ and run the command below in a terminal window

dotnet run

If you would like to run unit tests please run the command below

dotnet test

To run the angular frontend, in your terminal, go to the www directory and type in the following command

npm start

Please note the dotnet core sekeleton was based on the below blog posts

https://www.jeremywells.io/2020/06/11/starting-up-an-aspnetcore3-project.html