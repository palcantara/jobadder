//helper.service.ts
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
 
@Injectable({providedIn: 'root'})
export class HelperService {
 
  constructor(private http: HttpClient) {
    
  }
  getJobs() {
    return this.http.get('job')
  }
  getCandidatesWithSkills(skills) {
    return this.http.get('candidate?skills=' + skills)
  }
}