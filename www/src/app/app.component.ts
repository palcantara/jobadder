//app.component.ts
import { Component } from '@angular/core';
import { HelperService } from './_service/helper.service';
import {Candidate} from './models/candidate'
import {Job} from './models/job'
import {HttpClient} from '@angular/common/http';
import { ModalService } from './_service/modal.service';
import './_content/modal.less';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers:[HelperService]
})
export class AppComponent {
  
  jobName: String;
  companyName: String;
  
  title = 'JobAdder Jobs';
  candidates : Candidate[];
  jobs : Job[];
  constructor(public http: HttpClient, private helperService: HelperService,private modalService: ModalService){
  }
 
  ngOnInit(): void {
    this.getJobs();
  }
  getJobs() {
    this.helperService
    .getJobs()
    .subscribe((data:Job[]) => {
      this.jobs = data;
    });
  }
  getCandidatesWithSkills(skills) {
    this.helperService
    .getCandidatesWithSkills(skills)
    .subscribe((data:Candidate[]) => {
      console.log(data);
      this.candidates = data;
    });
  }
  openModal(id: string, selectedJob: Job) {
    this.jobName = selectedJob.name;
    this.companyName = selectedJob.company;
    this.getCandidatesWithSkills(selectedJob.skills);
    this.modalService.open(id);
  }
  closeModal(id: string) {
    this.modalService.close(id);
  }
}