using System;

namespace JobAdderWalkingSkeleton.Models
{
    public class Jobs
    {
        public int jobId { get; set; }
        public string name { get; set; }    
        public string company { get; set; }
        public string skills { get; set; }

    }
}