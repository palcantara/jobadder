using System;

namespace JobAdderWalkingSkeleton.Models
{
    public class Candidate
    {
        public int candidateId { get; set; }

        public string name { get; set; }
    
        public string skillTags { get; set; }
        public DateTime date { get; set; }
    }
}
