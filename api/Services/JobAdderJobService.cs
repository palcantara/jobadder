using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using JobAdderWalkingSkeleton.Models;
using JobAdderWalkingSkeleton.Infrastructure;

namespace JobAdderWalkingSkeleton.Services
{
    public interface IOpenJobAdderJobService
    {   
        Task<List<Jobs>> GetJobs();
    }   

    public class JobAdderJobService : IOpenJobAdderJobService {
        
        private readonly IHttpClientFactory _httpFactory;

        public JobAdderJobService(IHttpClientFactory httpFactory)
        {
            _httpFactory = httpFactory;
        }
        
        public async Task<List<Jobs>> GetJobs(){
            string url = $"https://private-76432-jobadder1.apiary-mock.com/jobs";
            
            var client = _httpFactory.CreateClient("JobAdderJobClient");
            var response = await client.GetAsync(url);
            var jobs = new List<Jobs>();

            if (response.IsSuccessStatusCode)
            {
                var json = response.Content.ReadAsStringAsync().Result;

                //Deserialize the response.
                var jobAdderJobResponse = JsonSerializer.Deserialize<List<Jobs>>(json);

                return jobAdderJobResponse;
            } else {
                throw new JobAdderException(response.StatusCode, "Error response from OpenWeatherApi: " + response.ReasonPhrase);
            }                


            

        }
    }

}