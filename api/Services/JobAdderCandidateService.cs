using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using JobAdderWalkingSkeleton.Models;
using JobAdderWalkingSkeleton.Infrastructure;

namespace JobAdderWalkingSkeleton.Services
{

    public interface IOpenJobAdderCandidateService
    {   
        Task<List<Candidate>> GetCandidatesWithSkills(string skills);
    }  
   
    public class  JobAdderCandidateService: IOpenJobAdderCandidateService {
        private readonly IHttpClientFactory _httpFactory;

        public JobAdderCandidateService(IHttpClientFactory httpFactory)
        {
            _httpFactory = httpFactory;
        }

        public async Task<List<Candidate>> GetCandidatesWithSkills(string skills){
            
            string url = $"https://private-76432-jobadder1.apiary-mock.com/candidates";
            
            var client = _httpFactory.CreateClient("JobAdderCandidateClient");
            var response = await client.GetAsync(url);
            var candidates = new List<Candidate>();
            
            var skillsArray =skills.Split(",");  

            if (response.IsSuccessStatusCode)
            {
                var json = response.Content.ReadAsStringAsync().Result;

                // 2. Deserialize the response.
                var jobAdderCandidateResponse = JsonSerializer.Deserialize<List<Candidate>>(json);

                foreach (var candidate in jobAdderCandidateResponse) {
                    for(int i = 0; i < skillsArray.Length; i++) {
                        if (candidate.skillTags.Contains(skillsArray[i])) {
                                candidates.Add(new Candidate
                                {
                                    candidateId = candidate.candidateId,
                                    name = candidate.name,
                                    skillTags = candidate.skillTags,
                                });
                        }
                        
                    }
                }

                return candidates;
            } else {
                throw new JobAdderException(response.StatusCode, "Error response from OpenWeatherApi: " + response.ReasonPhrase);
            }   
        }
            

        }
    }