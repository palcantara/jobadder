using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JobAdderWalkingSkeleton.Services;

namespace JobAdderWalkingSkeleton.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CandidateController : ControllerBase
    {

        private readonly ILogger<CandidateController> _logger;

        private readonly IOpenJobAdderCandidateService _candidateService;

        public CandidateController(ILogger<CandidateController> logger, IOpenJobAdderCandidateService candidateService)
        {
            _logger = logger;
            _candidateService = candidateService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string skills)
        {
            if (string.IsNullOrEmpty(skills))
                return BadRequest("skills parameter is missing");           
            try
            {
                var candidates = await _candidateService.GetCandidatesWithSkills(skills);
                return Ok(candidates);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
