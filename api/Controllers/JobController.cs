using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JobAdderWalkingSkeleton.Infrastructure;
using JobAdderWalkingSkeleton.Services;

namespace JobAdderWalkingSkeleton.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobController : ControllerBase
    {

        private readonly ILogger<JobController> _logger;
        private readonly IOpenJobAdderJobService _jobService;

        public JobController(ILogger<JobController> logger, IOpenJobAdderJobService weatherService)
        {
            _logger = logger;
            _jobService = weatherService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {           
            try
            {
                var job = await _jobService.GetJobs();
                return Ok(job);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}