using System;
using System.Net;

namespace JobAdderWalkingSkeleton.Infrastructure
{
    public class JobAdderException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        
        public JobAdderException() {  }

        public JobAdderException(HttpStatusCode statusCode)
            => StatusCode = statusCode;

        public JobAdderException(HttpStatusCode statusCode, string message) : base(message)
            => StatusCode = statusCode;

        public JobAdderException(HttpStatusCode statusCode, string message, Exception inner) : base(message, inner)
            => StatusCode = statusCode;
    }
}