using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using JobAdderWalkingSkeleton.Models;

namespace JobAdderWalkingSkeleton.Tests.Infrastructure
{
    public static class JobAdderJobResponses
    {
        public static StringContent OkResponse => BuildOkResponse();
        public static StringContent NotFoundResponse => BuildNotFoundResponse();
        public static StringContent InternalErrorResponse => BuildInternalErrorResponse();

        private static StringContent BuildOkResponse()
        {
            var response = new List<Jobs>
            {
                    new Jobs {
                        jobId = 1,
                        name = "Test Job 1",
                        company = "Test company",
                        skills = "sales, problem-solving, detail, reception, aws"
                    },
            };
            var json = JsonSerializer.Serialize(response);
            return new StringContent(json);
        }

        private static StringContent BuildInternalErrorResponse()
        {
            var json = JsonSerializer.Serialize(new {Cod = 500, Message = "Internal Error."});
            return new StringContent(json);
        }

        private static StringContent BuildNotFoundResponse()
        {
            var json = JsonSerializer.Serialize(new { Cod = 404, Message = "city not found" });
            return new StringContent(json);
        }
    }
}