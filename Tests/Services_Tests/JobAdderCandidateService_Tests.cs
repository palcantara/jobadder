using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using JobAdderWalkingSkeleton.Models;
using JobAdderWalkingSkeleton.Tests.Infrastructure;
using Xunit;

namespace JobAdderWalkingSkeleton.Services
{
    public class JobAdderCandidateService_Tests
    {
        [Fact]
        public async Task Returns_A_ListOfCandidatesWithSkills()
        {            
            var clientFactory = ClientBuilder.JobAdderClientFactory(JobAdderCandidateResponses.OkResponse);
            var sut = new JobAdderCandidateService(clientFactory);
            
            var result = await sut.GetCandidatesWithSkills("sales, problem-solving, detail, reception, aws");

            Assert.IsType<List<Candidate>>(result);
        }
    }
}