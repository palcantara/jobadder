using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using JobAdderWalkingSkeleton.Controllers;
using JobAdderWalkingSkeleton.Models;
using JobAdderWalkingSkeleton.Services;
using JobAdderWalkingSkeleton.Tests.Infrastructure;
using Xunit;

namespace JobAdderWalkingSkeleton.Controllers
{
    public class JobController_Tests
    {
        [Fact]
        public async Task Returns_OkResult_With_Jobslist()
        {           
            var clientFactory = ClientBuilder.JobAdderClientFactory(JobAdderJobResponses.OkResponse);
            var service = new JobAdderJobService(clientFactory);
            var sut = new JobController(new NullLogger<JobController>(), service);

            var result = await sut.Get() as OkObjectResult;

            Assert.IsType<List<Jobs>>(result.Value);
            Assert.Equal(200, result.StatusCode);
        }
    }
}