using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using JobAdderWalkingSkeleton.Models;
using JobAdderWalkingSkeleton.Tests.Infrastructure;
using Xunit;

namespace JobAdderWalkingSkeleton.Services
{
    public class JobAdderJobService_Tests
    {
        [Fact]
        public async Task Returns_A_JobsList()
        {            
            var clientFactory = ClientBuilder.JobAdderClientFactory(JobAdderJobResponses.OkResponse);
            var sut = new JobAdderJobService(clientFactory);
            
            var result = await sut.GetJobs();

            Assert.IsType<List<Jobs>>(result);
        }
    }
}