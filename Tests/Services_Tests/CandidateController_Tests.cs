using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using JobAdderWalkingSkeleton.Controllers;
using JobAdderWalkingSkeleton.Models;
using JobAdderWalkingSkeleton.Services;
using JobAdderWalkingSkeleton.Tests.Infrastructure;
using Xunit;

namespace JobAdderWalkingSkeleton.Controllers
{
    public class CandidateController_Tests
    {
        [Fact]
        public async Task Returns_OkResult_With_CandidatesMatchingSkills()
        {           
            var clientFactory = ClientBuilder.JobAdderClientFactory(JobAdderCandidateResponses.OkResponse);
            var service = new JobAdderCandidateService(clientFactory);
            var sut = new CandidateController(new NullLogger<CandidateController>(), service);

            var result = await sut.Get("sales, problem-solving, detail, reception, aws") as OkObjectResult;

            Assert.IsType<List<Candidate>>(result.Value);
            Assert.Equal(200, result.StatusCode);
        }
    }
}